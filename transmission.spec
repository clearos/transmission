Name:           transmission
Version:        2.84
Release:        1%{?dist}
Summary:        A lightweight GTK+ BitTorrent client
Group:          Applications/Internet
# See COPYING. This licensing situation is... special.
License:        MIT and GPLv2
URL:            http://www.transmissionbt.com/
Source0:		http://mirrors.m0k.org/transmission/files/transmission-%{version}.tar.xz
Source1:        transmission-daemon-init
Source2:        transmission-daemon-sysconfig
Source3:		transmission-daemon-logrotate
Source4:		transmission-password
BuildRequires:  openssl-devel >= 0.9.4
BuildRequires:  glib2-devel
BuildRequires:  curl-devel
BuildRequires:  dbus-glib-devel >= 0.70
BuildRequires:  libevent2-devel
BuildRequires:  GConf2-devel
# Disable translations for now -- requires more recent version of intltool 
# ./configure --disable-nls
#BuildRequires:  gettext intltool
BuildRoot: /%{_tmppath}/%{name}-%{version}-%{release}-root
Requires: transmission-cli = %version-%release
Requires: transmission-daemon = %version-%release
Requires: logrotate

%description
Transmission is a free, lightweight BitTorrent client. It features a
simple, intuitive interface on top on an efficient, cross-platform
back-end.

%package common
Summary:       Transmission common files
Group:         Applications/Internet
Conflicts:     transmission < 1.80-0.3.b4
%description common
Common files for Transmission BitTorrent client sub-packages. It includes 
the web user interface, icons and transmission-remote utility.

%package cli
Summary:       Transmission command line implementation
Group:         Applications/Internet
Requires:      transmission-common = %version-%release
#Provides:      transmission = %{version}-%{release}
%description cli
Command line version of Transmission BitTorrent client.

%package daemon
Summary:       Transmission daemon
Group:         Applications/Internet
Requires:      transmission-common = %version-%release
Requires(pre): shadow-utils
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts
Requires(postun): initscripts
#Provides:      transmission = %{version}-%{release}
%description daemon
Transmission BitTorrent client daemon.

%pre daemon
getent group transmission >/dev/null || groupadd -r transmission
getent passwd transmission >/dev/null || \
useradd -r -g transmission -d /var/lib/transmission -s /sbin/nologin \
        -c "transmission daemon account" transmission
exit 0

%prep
%setup -q 

%build
export LDFLAGS="$LDFLAGS -lstdc++"
%configure --disable-static --disable-gtk --enable-daemon --disable-nls
make %{?_smp_mflags}

%install
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d
mkdir -p %{buildroot}%{_sysconfdir}/rc.d/init.d
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_localstatedir}/lib/transmission
mkdir -p %{buildroot}%{_localstatedir}/log/transmission

install -m755 %{SOURCE1} %{buildroot}%{_sysconfdir}/rc.d/init.d/transmission-daemon
install -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/transmission-daemon
install -m644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/transmission-daemon
install -m755 %{SOURCE4} %{buildroot}%{_bindir}/transmission-password

make install DESTDIR=%{buildroot}

#%find_lang %{name}

%clean
rm -rf %{buildroot}

%post daemon
/sbin/chkconfig --add transmission-daemon

%preun daemon
if [ $1 = 0 ] ; then
    /sbin/service transmission-daemon stop >/dev/null 2>&1
    /sbin/chkconfig --del transmission-daemon
fi

%postun daemon
if [ "$1" -ge "1" ] ; then
    /sbin/service transmission-daemon condrestart >/dev/null 2>&1 || :
fi

%files 

%files common 
%defattr(-, root, root, -)
%doc AUTHORS COPYING NEWS README
%{_bindir}/transmission-remote
%{_bindir}/transmission-edit
%{_bindir}/transmission-create
%{_bindir}/transmission-show
%{_datadir}/transmission/web/
%doc %{_mandir}/man1/transmission-remote*
%doc %{_mandir}/man1/transmission-edit*
%doc %{_mandir}/man1/transmission-create*
%doc %{_mandir}/man1/transmission-show*

%files cli 
%defattr(-, root, root, -)
%{_bindir}/transmission-cli
%doc %{_mandir}/man1/transmission-cli*

%files daemon
%defattr(-, root, root, -)
%{_bindir}/transmission-daemon
%{_bindir}/transmission-password
%{_sysconfdir}/logrotate.d/transmission-daemon
%{_sysconfdir}/rc.d/init.d/transmission-daemon
%{_sysconfdir}/sysconfig/transmission-daemon
%dir %attr(-,transmission, transmission)%{_localstatedir}/lib/transmission/
%dir %attr(-,transmission, transmission)%{_localstatedir}/log/transmission/
%doc %{_mandir}/man1/transmission-daemon*

%changelog
* Wed Sep 16 2015 Tim Burgess <timb80@yahoo.com> - 2.84-1
- Update to 2.84

* Tue Oct 01 2013 Tim Burgess <timb80@yahoo.com> - 2.82-1
- Update to 2.82

* Thu Jan 24 2013 Tim Burgess <timb80@yahoo.com> - 2.75-4
- Add version to dependancy for transmission-common to fix upgrade problem

* Mon Jan 22 2013 Tim Burgess <timb80@yahoo.com> - 2.75-3
- Tidy up and fix init script for duplicate PID

* Thu Jan 17 2013 Tim Burgess <timb80@yahoo.com> - 2.75-2
- Remove transmission-cli and transmission-daemon provide entries so that transmission package pulls in all dependancies

* Tue Jan 01 2013 Tim Burgess <timb80@yahoo.com> - 2.75-1
- Update to 2.75

* Mon Jun 04 2012 Tim Burgess <timb80@yahoo.com> - 2.52-1
- Update to 2.52

* Tue Mar 27 2012 Tim Burgess <timb80@yahoo.com> - 2.50-1
- Update to 2.50

* Wed Oct 23 2011 Tim Burgess <timb80@yahoo.com> - 2.42-1
- Update to 2.42

* Mon Jun 06 2011 Tim Burgess <timb80@yahoo.com> - 2.22-2
- Fix init script and transmission-password

* Mon Apr 18 2011 Tim Burgess <timb80@yahoo.com> - 2.22-1
- Update to 2.22

* Sat Mar 20 2010 Peter Baldwin <pbaldwin@clearfoundation.com> - 1.92-1
- Import from Fedora and prepped for ClearOS 5.x
- Change to headless build only

* Wed Feb 17 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.90-1
- http://trac.transmissionbt.com/browser/trunk/NEWS#L1
- Fix initscript to use the config file properly

* Wed Feb 10 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.83-3
- rhbz #563090 - fixed config for daemon initscript

* Wed Feb 3 2010 Ankur Sinha <ankursinha@fedoraproject.org> - 1.83-2
- Bugfix - #560180 - changed init script

* Sun Jan 31 2010 Ankur Sinha <ankursinha@fedoraproject.org> - 1.83-1
- New Release
- Fix 1.80 announce error that caused uploads and downloads to periodically freeze
- Fix 1.80 announce timeout error that caused "no response from tracker" message
- Fix 1.80 "file not found" error message that stopped some torrents
- Fix 1.82 crash when adding new torrents via their ftp URL
- Fix 1.80 crash when receiving invalid request messages from peers
- Fix 1.82 error when updating the blocklist
- http://trac.transmissionbt.com/wiki/Changes#version-1.83

* Mon Jan 25 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.82-2
- Fix icon cache 

* Sun Jan 24 2010 Ankur Sinha <ankursinha@fedoraproject.org> - 1.82-1
- Bugfix
- http://trac.transmissionbt.com/wiki/Changes#version-1.82

* Thu Jan 21 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.80-1
- Many major new features including magnet link support, trackerless torrents
- http://trac.transmissionbt.com/wiki/Changes#version-1.80

* Wed Jan 20 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.80-0.6.b5
- Add a initscript for transmission daemon. Fixes rhbz#556228
- Description changes, add group for sub-packages and fix make

* Thu Jan 14 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.80-0.5.b5
- Bug fixes
- http://trac.transmissionbt.com/wiki/Changes#version-1.80b5

* Sat Jan 09 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.80-0.4.b4
- Build the qt interface as a sub package
- Build daemon as a separate sub package
- Translations are for only the gtk sub package
- Fix obsoletes and add conflicts

* Thu Jan 07 2010 Ankur Sinha <ankursinha AT fedoraproject DOT org> - 1.80-0.3.b4
- Split package to sub packages

* Tue Jan 05 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.80-0.2.b4
- Add BR GConf2-devel

* Tue Jan 05 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 1.80-0.1.b4
- 1.80 Beta 4
- http://trac.transmissionbt.com/wiki/Changes#version-1.80b4

* Thu Dec 17 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 1.80-0.1.b3
- 1.80 Beta 3
- Enable sounds via libcanberra
- http://trac.transmissionbt.com/wiki/Changes#version-1.80b3

* Sun Oct 25 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 1.76-1
- http://trac.transmissionbt.com/wiki/Changes#version-1.76

* Tue Sep 15 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 1.75-1
- new upstream release
- Fixes seg fault, rhbz#522783

* Thu Aug 27 2009 Tomas Mraz <tmraz@redhat.com> - 1.74-3
- rebuilt with new openssl

* Tue Aug 25 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 1.74-2
- Add source

* Tue Aug 25 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 1.74-1
- Bug fix release
- http://trac.transmissionbt.com/wiki/Changes
- disable static linking explicitly

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.73-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jul 24 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 1.73-1
- new upstream
- switch to using LZMA source

* Sun Jun 21 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 1.72-1
- Update to new upstream version
- Drop compiler options patch since upstream has fixed this issue

* Fri Jun 12 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 1.71-1
- Update to upstream version 1.71
- Update compiler options patch to match new upstream release
- Drop patch for not using bundled libevent. Upstream now has been fixed to use the system copy whenever possible
- Don't use vendor tag for desktop file. It is not recommended anymore
- Follow https://fedoraproject.org/wiki/Packaging/Guidelines#All_patches_should_have_an_upstream_bug_link_or_comment

* Thu May 28 2009 Denis Leroy <denis@poolshark.org> - 1.61-1
- Update to upstream version 1.61
- fallocate patch upstreamed
- Patches updated for 1.61

* Fri May 22 2009 Denis Leroy <denis@poolshark.org> - 1.53-1
- Update to upstream 1.53
- XDG Download patch upstreamed
- Security fix CVE-2009-1757 (#500278)

* Sat Mar 28 2009 Lubomir Rintel <lkundrak@v3.sk> - 1.51-2
- Use XDG Download directory (#490950)

* Sat Feb 28 2009 Denis Leroy <denis@poolshark.org> - 1.51-1
- Update to upstream 1.51
- Added icon cache scriplets (#487824)

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.50-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Feb 20 2009 Denis Leroy <denis@poolshark.org> - 1.50-1
- Update to upstream 1.50
- Ported patches to 1.50, enforce compile flags

* Sun Jan 18 2009 Tomas Mraz <tmraz@redhat.com> - 1.42-2
- rebuild with new openssl

* Wed Dec 31 2008 Brian Pepple <bpepple@fedoraproject.org> - 1.42-1
- Update to 1.42.
- Update event patch to 1.42.

* Fri Nov 21 2008 Denis Leroy <denis@poolshark.org> - 1.40-1
- Update to upstream 1.40
- Ported patches to 1.40

* Sun Sep 28 2008 Denis Leroy <denis@poolshark.org> - 1.34-1
- Update to upstream 1.34
- Added patch to link with distributed libevent library

* Mon Sep  8 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.33-2
- fix license tag

* Sun Aug 24 2008 Denis Leroy <denis@poolshark.org> - 1.33-1
- Update to upstream 1.33
- Now dual-licensed
- Gnusource and download dir patches upstreamed

* Wed Jun 18 2008 Denis Leroy <denis@poolshark.org> - 1.22-1
- Update to upstream 1.22

* Sat May 31 2008 Denis Leroy <denis@poolshark.org> - 1.21-1
- Update to upstream 1.21

* Tue May 13 2008 Denis Leroy <denis@poolshark.org> - 1.20-1
- Update to upstream 1.20
- Browser opening patch upstreamed
- New dependencies (dbus, curl)

* Tue May  6 2008 Denis Leroy <denis@poolshark.org> - 1.11-2
- Patch to fix opening issue from browser (#431769)
- Patch to fix hardcoded optimize compile flags

* Fri May  2 2008 Denis Leroy <denis@poolshark.org> - 1.11-1
- Update to upstream 1.11, many bug fixes

* Fri Mar 14 2008 Denis Leroy <denis@poolshark.org> - 1.06-1
- Update to upstream 1.06, bug fixes, memory leak fix

* Sun Feb 10 2008 Denis Leroy <denis@poolshark.org> - 1.05-1
- Update to upstream 1.05, with a bunch of bug fixes

* Thu Jan 31 2008 Denis Leroy <denis@poolshark.org> - 1.03-1
- Update to upstream 1.03

* Wed Jan 23 2008 Denis Leroy <denis@poolshark.org> - 1.02-1
- Update to upstream 1.02, bugfix release

* Sat Jan  5 2008 Denis Leroy <denis@poolshark.org> - 1.00-1
- Update to upstream 1.00. New project URL

* Wed Dec  5 2007 Denis Leroy <denis@poolshark.org> - 0.95-1
- Update to upstream 0.95
- Rebuild with new openssl

* Thu Nov 29 2007 Denis Leroy <denis@poolshark.org> - 0.94-1
- Update to upstream 0.94

* Tue Nov  6 2007 Denis Leroy <denis@poolshark.org> - 0.92-1
- Update to upstream 0.92, important bug fixes

* Sat Nov  3 2007 Denis Leroy <denis@poolshark.org> - 0.91-1
- Update to upstream 0.91
- Removal of -gtk suffix
- Obsoleting manpath patch

* Wed Sep 12 2007 Denis Leroy <denis@poolshark.org> - 0.82-1
- Update to upstream 0.82, many bug fixes
- Added patch to support default user download directory (Bastien Nocera)

* Sat Aug 25 2007 - Bastien Nocera <bnocera@redhat.com> - 0.81-1
- Update to upstream 0.81
- Add work-around for busted tarball without a sub-directory

* Thu Aug 16 2007 Denis Leroy <denis@poolshark.org> - 0.80-1
- Update to upstream 0.80

* Wed May  2 2007 Denis Leroy <denis@poolshark.org> - 0.72-1
- Update to 0.72
- Added libevent BR

* Wed Apr 25 2007 Denis Leroy <denis@poolshark.org> - 0.71-1
- Update to 0.71
- Removed custom desktop file
- Added patch to fix manpath

* Thu Sep 28 2006 Denis Leroy <denis@poolshark.org> - 0.6.1-3
- Added project icon
- Honor cc variable

* Mon Sep 25 2006 Denis Leroy <denis@poolshark.org> - 0.6.1-2
- Removed ldconfig Requires

* Wed Sep 13 2006 Denis Leroy <denis@poolshark.org> - 0.6.1-1
- First version

